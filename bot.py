import aiosqlite
import asyncio
import dhash
import hashlib
import logging
import os
import random
import re
import requests
import sentry_sdk
import sqlite3
import time
from collections import defaultdict
from datetime import datetime, timedelta
from dotenv import load_dotenv
from google.cloud import translate_v2
from io import BytesIO, StringIO

import telethon.errors
from PIL import Image
from telethon import TelegramClient, events, Button
from telethon.tl.types import (
    ChannelParticipantsAdmins,
    InputMessagesFilterPhotos,
    MessageEntityMention,
    DocumentAttributeFilename,
    User,
    Chat,
    Channel
)
from telethon.errors import MessageIdInvalidError, ChatAdminRequiredError, ChannelPrivateError
from telethon.events import StopPropagation
from telethon.tl.functions.users import GetFullUserRequest

logging.basicConfig(
    format="[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

sentry_sdk.init(
    "https://c87ddb61c10f40b19d4ebf5ccd5a2968@sentry.yaoihavenreborn.com/4",
    traces_sample_rate=1.0
)

load_dotenv()

api_id = os.environ.get("api_id")
api_hash = os.environ.get("api_hash")
bot_token = os.environ.get("bot_token")

translate_client = translate_v2.Client.from_service_account_json('translateapicreds.json')

last_hash = {}

raid_mode = defaultdict(bool)

last_ten_hash = defaultdict(list)

sent_sticker_count = defaultdict(int)

duplicate_messages = [
    "Whoops, looks like that was a duplicate! I deleted it for you.",
    "I swear I just saw that one, I'll go ahead and delete it for you",
    "Repost, deleted!",
    "Sorry buddy, somebody sent that earlier today. Deyeeted.",
    "Get that old shit outta here",
    "Oh look, this one again",
    "I think you accidentally reposted. I accidentally deleted your shit.",
    "I know it's good, but we seen it",
    "Once is enough, my dude",
    "Check the chat to be sure you're not sending dupes next time",
]

global_event_chats = [
    -1001290516227,  # Shota Bureau of Investigation
    -1001431785686,  # Snep Mods
    -1001462025007,  # Shota Intelligence Agency (Steph's)
]

restricted_users = [
    189413770
]

gban_users = [
    100927064,  # Atoro
    774271888,  # Darkkita
    248702290,  # Cubby
    842968283,  # Wolfy
    488720381,  # Milo the Lion
    348356700,  # ManyRedPandas,
    5463709547, # Simon
]

bot = TelegramClient("bot", int(api_id), api_hash).start(bot_token=bot_token)


# @bot.on(events.Album())
# async def check_album(event):
#     logging.info(f"Album with {len(event)} pics sent\n{event.raw_text=}")
#     raise StopPropagation


async def is_admin(event, user):
    try:
        if user is None:
            return False
        if user.id == 100927064:
            return True
        if user.id in restricted_users:
            return False
        chat = await event.get_chat()
        perms = await bot.get_permissions(chat, user)
        return perms.is_admin
    except Exception as e:
        sentry_sdk.capture_exception(e)
        return False


async def get_admins(chat):
    admins = await bot.get_participants(chat, filter=ChannelParticipantsAdmins)
    admin_list = []
    for admin in admins:
        if admin.deleted or admin.bot:
            continue
        if admin.username:
            admin_list.append("@" + admin.username)
        else:
            name = admin.first_name
            if admin.last_name:
                name += " " + admin.last_name
            admin_list.append(name)
    return admin_list


async def get_welcome_message(chat, member):
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT welcome, send_welcome FROM servers WHERE chat_id=?", (chat.id,)
        )
        row = await cursor.fetchone()
        if not row:
            await insert_server(db, chat)
            return False
        if not row[1]:
            return False

        admins = await get_admins(chat)

        return (
            row[0]
            .replace("%first_name", member.first_name or "")
            .replace("%last_name", member.last_name or "")
            .replace(
                "%mention",
                "@" + member.username
                if member.username
                else f"<a href='tg://user?id={member.id}'>{member.first_name or ''} {member.last_name or ''}</a>",
            )
            .replace("%chat_name", chat.title or chat.username)
            .replace("%admins", ", ".join(admins))
        )


async def get_rules(chat_id):
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT rules FROM rules WHERE chat_id=?", (chat_id,)
        )
        row = await cursor.fetchone()
        if not row:
            return None

        return row[0]


async def insert_server(db, chat):
    await db.execute(
        """
        INSERT INTO servers(chat_id, welcome, send_welcome, send_snark) VALUES (?, ?, ?, ?)
        ON CONFLICT(chat_id) DO NOTHING
    """,
        (chat.id, "Welcome to the server, %mention!", 1, 1),
    )
    await db.commit()


async def insert_rules(db, chat_id):
    await db.execute(
        """
        INSERT INTO rules(chat_id) VALUES (?)
        ON CONFLICT(chat_id) DO NOTHING
        """,
        (chat_id,)
    )
    await db.commit()


@bot.on(events.NewMessage(pattern="/start"))
async def start(event):
    """Sends a message when the command /start is issued"""
    await event.respond(
        "Hi there, I'm YHR Bot!\n"
        "For a detailed list of all my commands, check out https://telegram.yhrbot.com"
    )
    raise events.StopPropagation


@bot.on(events.ChatAction)
async def handler(event):
    if event.user_joined or event.user_added:
        chat = await event.get_chat()
        user = await event.get_user()

        banned = await check_unseen_bans(user, chat)
        if banned:
            return

        welcome_message = await get_welcome_message(chat, user)
        if welcome_message:
            await event.respond(welcome_message, parse_mode="html")
        async with aiosqlite.connect("db2.sqlite3") as db:
            cursor = await db.execute(
                "SELECT check_newcomers FROM servers WHERE chat_id=?", (chat.id,)
            )
            row = await cursor.fetchone()
            value = row[0]
        if value == 1:
            await check_newcomers_for_sc_and_zoo(event, user)
        logging.info(
            f"[{chat.title}] User {user.first_name if user.first_name else ''} {user.last_name if user.last_name else ''} (@{user.username if user.username else 'None'}) joined"
        )
        if raid_mode[event.chat_id]:
            await bot.edit_permissions(event.chat_id, user, timedelta(minutes=120), send_media=False)


@bot.on(events.NewMessage(pattern="/rules"))
async def send_rules(event):
    """
    Sends rules set up by admins
    """
    rules = await get_rules(event.chat_id)
    if rules:
        await event.reply(rules, parse_mode="html")


@bot.on(events.NewMessage(pattern="/update_rules"))
async def update_rules(event):
    """
    Updates the rules stored in the DB for the chat
    """
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_rules(db, event.chat_id)
        await db.execute(
            "UPDATE rules SET rules=? WHERE chat_id=?",
            (event.message.message.split(" ", 1)[1], event.chat_id),
        )
        await db.commit()
    await event.reply("Rules updated!")
    chat = await event.get_chat()
    logging.info(f"[{chat.title}] Rules updated")


@bot.on(events.NewMessage(pattern="/ping"))
async def ping(event):
    """Replies with PONG - for testing bot connectivity"""
    await event.reply("Pong!")


@bot.on(events.NewMessage(pattern="/test_welcome"))
async def test_welcome(event):
    """Tests the welcome message in a given chat"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    sender = await event.get_sender()
    welcome_message = await get_welcome_message(chat, sender)
    if welcome_message:
        await bot.send_message(chat.id, welcome_message, parse_mode="html")
    logging.info(
        f"[{chat.title if chat.title else chat.username}] Welcome message tested"
    )


@bot.on(events.NewMessage(pattern="/toggle_welcome"))
async def toggle_welcome(event):
    """Toggles welcome messages on and off on a per-channel basis"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_server(db, chat)
        await db.execute(
            """
            UPDATE servers SET send_welcome = CASE
                WHEN send_welcome = 1 THEN 0
                ELSE 1
                END
            WHERE chat_id=?
        """,
            (chat.id,),
        )
        await db.commit()
        cursor = await db.execute(
            "SELECT send_welcome FROM servers WHERE chat_id=?", (chat.id,)
        )
        value = await cursor.fetchone()
        value = value[0]
    await event.reply(f"Welcome message toggled {'on' if value == 1 else 'off'}!")
    logging.info(
        f"[{chat.title if chat.title else chat.username}] Welcome toggled {'on' if value == 1 else 'off'}"
    )


@bot.on(events.NewMessage(pattern="/toggle_snark"))
async def toggle_snark(event):
    """Toggles snarky replies to dupes on and off on a per-channel basis"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_server(db, chat)
        await db.execute(
            """
            UPDATE servers SET send_snark = CASE
                WHEN send_snark = 1 THEN 0
                ELSE 1
                END
            WHERE chat_id=?
        """,
            (chat.id,),
        )
        await db.commit()
        cursor = await db.execute(
            "SELECT send_snark FROM servers WHERE chat_id=?", (chat.id,)
        )
        value = await cursor.fetchone()
        value = value[0]
    await event.reply(
        f"Snarky replies to dupes toggled {'on' if value == 1 else 'off'}!"
    )
    logging.info(f"[{chat.title}] Snark toggled {'on' if value == 1 else 'off'}")


@bot.on(events.NewMessage(pattern="/update_welcome"))
async def update_welcome(event):
    """Updates the welcome message on a per-channel basis"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    msg = event.message.message[16:]
    if msg.lower().startswith("yhr_bot"):
        msg = msg[7:].strip()

    if not msg:
        return await event.reply("You gotta give me something to actually welcome people with =/")

    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_server(db, chat)
        await db.execute(
            "UPDATE servers SET welcome=? WHERE chat_id=?",
            (event.message.message[16:], chat.id),
        )
        await db.commit()
    await event.reply("Welcome message updated!")
    logging.info(f"[{chat.title}] Welcome message updated")


async def get_image_hash(message):
    image = None
    if message.fwd_from:
        if isinstance(message.fwd_from.from_id, telethon.types.PeerUser):
            if message.fwd_from.from_id.user_id == 619578907:
                try:
                    logging.info("Forward from E621")
                    url = message.entities[3].url
                    logging.info(f"{url=}")
                    r = requests.get(url)
                    image = Image.open(BytesIO(r.content))
                except Exception as e:
                    logging.error(f"Error with get_image_hash: {e}")
    if image is None:
        raw_image = await message.download_media(file=BytesIO())
        image = Image.open(raw_image)
    if not image:
        return
    row, col = dhash.dhash_row_col(image)
    img_hash = dhash.format_hex(row, col)
    return str(int(img_hash, 16))


async def add_to_hash_list(img_hash, chat):
    last_ten_hash[chat].append(img_hash)
    if len(last_ten_hash[chat]) > 10:
        last_ten_hash[chat].pop(0)


@bot.on(events.NewMessage(pattern="/dhash"))
async def show_hash(event):
    chat = await event.get_chat()
    sender = await event.get_sender()
    logging.info(
        f"[{chat.title}] {sender.username if sender.username else (sender.first_name if sender.first_name else '')}"
        f" ran /dhash..."
    )
    reply = await event.get_reply_message()
    if not event.message.photo and reply is None:
        logging.info(
            f"[{chat.title}] {sender.username if sender.username else (sender.first_name if sender.first_name else '')}"
            f" no image in og message or reply, done"
        )
        return
    if event.message.photo:
        raw_img = await event.message.download_media(file=BytesIO())
    else:
        raw_img = await reply.download_media(file=BytesIO())
    image = Image.open(raw_img)
    dhash_int = dhash.dhash_int(image)
    matrix = dhash.format_matrix(dhash_int, bits=".*")
    await event.reply(f"```\n{matrix}\n```")
    raise events.StopPropagation


@bot.on(events.NewMessage(pattern="/dupes"))
async def toggle_dupe_checks(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_server(db, chat)
        await db.execute(
            """
            UPDATE servers SET check_dupes = CASE
                WHEN check_dupes = 1 THEN 0
                ELSE 1
                END
            WHERE chat_id=?
        """,
            (chat.id,),
        )
        await db.commit()
        cursor = await db.execute(
            "SELECT check_dupes FROM servers WHERE chat_id=?", (chat.id,)
        )
        value = await cursor.fetchone()
        value = value[0]
    await event.reply(
        f"Duplicate image/gif checking turned {'on' if value == 1 else 'off'}!"
    )
    logging.info(f"[{chat.title}] Duplicate image/gif checking turned {'on' if value == 1 else 'off'}")


@bot.on(events.NewMessage())
async def check_duplicate_images(event):
    if not event.message.photo:
        try:
            from_id = event.message.fwd_from.from_id.user_id
        except AttributeError:
            return
        if from_id != 619578907:
            return
    chat = await event.get_chat()
    if isinstance(chat, User):
        return
    else:
        title = chat.title
    sender = await event.get_sender()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return logging.info(f"[{chat.title}] I do not have perms in {chat.title}")
    async with aiosqlite.connect(
        "db2.sqlite3", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    ) as db:
        cursor = await db.execute(
            "SELECT check_dupes FROM servers WHERE chat_id=?",
            (chat.id,)
        )
        check_dupes = await cursor.fetchone()

        if sender is not None and sender.username:
            cursor = await db.execute(
                "SELECT COUNT(*) FROM ignore WHERE chat_id=? AND username=?",
                (chat.id, sender.username),
            )
            count = await cursor.fetchone()
            if count[0] > 0:
                logging.info(f"[{title}] Ignoring image sent by {sender.username}")
                return
        img_hash_str = await get_image_hash(event.message)

        if not img_hash_str:
            return sentry_sdk.capture_message(f"Error getting image hash from message:\n{event.message}")

        if await check_banned_image(event, db):
            return

        try:
            if last_hash[chat.id] == img_hash_str:
                return
            else:
                last_hash[chat.id] = img_hash_str

            # Check if the image has been sent within the last 10 messages
            # This could be because of an out of control repost loop, so we
            # disable sending of the user for 35 seconds
            if img_hash_str in last_ten_hash[chat.id]:
                logging.warning(f"Potential repost loop detected, limiting {sender.username or sender.first_name} "
                                "for 35 seconds...")
                return await bot.edit_permissions(event.chat_id, sender, timedelta(seconds=35), send_media=False)

            await add_to_hash_list(img_hash_str, chat.id)
        except KeyError:
            last_hash[chat.id] = img_hash_str
        if sender is not None:
            name = f"Unknown User ({sender.id})"
            if hasattr(sender, "username"):
                name = sender.username
            elif hasattr(sender, "first_name"):
                if sender.first_name:
                    name = sender.first_name
                if sender.last_name:
                    name += " " + sender.last_name
            logging.info(
                f"[{title}] {name} sent an image. Looking for duplicate..."
            )
        cursor = await db.execute(
            "SELECT img_hash, sent_on, seen, original FROM images WHERE chat_id=?",
            (chat.id, )
        )
        all_images = await cursor.fetchall()

        # Moving dupe check to here so we can still do ban check
        if not check_dupes[0]:
            return
        new = True
        for image in all_images:
            diff = dhash.get_num_bits_different(int(img_hash_str), int(image[0]))
            if diff > 3:
                continue
            new = False

            # Iterate seen counter
            seen = int(image[2]) + 1
            await db.execute("UPDATE images SET seen=? WHERE img_hash=? AND chat_id=?",
                             (seen, img_hash_str, chat.id))
            await db.commit()

            # if image[1] + timedelta(hours=24) < datetime.now():
            #     logging.info(
            #         f"[{chat.title}] Duplicate found, older, deleting and sending original"
            #     )
            #     try:
            #         await bot.forward_messages(
            #             entity=chat.id, messages=int(image[3]), from_peer=chat.id
            #         )
            #         await asyncio.sleep(5)
            #         return await event.delete()
            #     except MessageIdInvalidError:
            #         await db.execute(
            #             "UPDATE images SET original=? WHERE img_hash=? AND chat_id=?",
            #             (event.message.id, img_hash_str, chat.id),
            #         )
            #         await db.commit()
            #         return logging.info(
            #             f"[{chat.title}] Original was deleted, setting this message as original"
            #         )
            # else:
            if image[1] + timedelta(hours=24) > datetime.now():
                logging.info(
                    f"[{title}] Duplicate found, recent, deleting and chastising"
                )
                cursor2 = await db.execute(
                    "SELECT send_snark FROM servers WHERE chat_id=?", (chat.id,)
                )
                row = await cursor2.fetchone()
                value = row[0]
                if value == 1:
                    seen = int(image[2]) + 1
                    message = await bot.get_messages(entity=chat, ids=int(image[3]))
                    if message is None:
                        await db.execute(
                            "UPDATE images SET original=? WHERE img_hash=? AND chat_id=?",
                            (event.message.id, img_hash_str, chat.id),
                        )
                        await db.commit()
                        return logging.info(
                            f"[{title}] Original was deleted, setting this message as original"
                        )
                    await bot.send_message(
                        entity=chat,
                        message=random.choice(duplicate_messages)
                        + f" ({seen} time{'s' if seen > 1 else ''})",
                    )
                await bot.send_message(
                    entity=chat,
                    reply_to=int(image[3]),
                    message="Here's the original",
                )
                await asyncio.sleep(5)
                return await event.delete()
            else:
                await db.execute(
                    "UPDATE images SET original=? WHERE img_hash=? AND chat_id=?",
                    (event.message.id, img_hash_str, chat.id),
                )
                await db.commit()
                return logging.info(
                    f"[{title}] Original was old, setting this message as new original"
                )
        if new:
            logging.info(f"[{title}] Not a duplicate, adding to DB")
            await db.execute(
                "INSERT INTO images(img_hash, original, sent_on, chat_id, seen)  VALUES (?, ?, ?, ?, 1)",
                (img_hash_str, event.message.id, datetime.now(), chat.id),
            )
            await db.commit()
            return


async def check_banned_image(event, db):
    new_img_hash = await get_image_hash(event.message)
    rows = await db.execute("SELECT img_hash, id FROM banned_images WHERE chat_id=? OR chat_id is NULL", (event.chat_id, ))
    rows = await rows.fetchall()
    for row in rows:
        diff = dhash.get_num_bits_different(int(new_img_hash), int(row[0]))
        if diff <= 3:
            if event.message.message == "/imgunban":
                await db.execute("DELETE FROM banned_images WHERE id=?", (row[1],))
                await event.respond("Image unbanned")
                return True
            await event.delete()
            return True
    return False


@bot.on(events.NewMessage(pattern="/imgban"))
async def ban_img(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    perms = await bot.get_permissions(event.chat_id, 'me')
    if not perms.is_admin:
        return
    msg = await event.get_reply_message()
    if msg.photo:
        img_hash = await get_image_hash(msg)
        async with aiosqlite.connect("db2.sqlite3") as db:
            await db.execute("INSERT INTO banned_images(img_hash, chat_id) VALUES(?, ?)", (img_hash, event.chat_id))
            await db.commit()
            await msg.delete()
            await event.delete()
            return await event.respond("Image banned")
    elif msg.gif:
        gif_hash = await get_gif_hash(msg)
        async with aiosqlite.connect("db2.sqlite3") as db:
            await db.execute("INSERT INTO banned_gifs(gif_hash, chat_id) VALUES(?, ?)", (gif_hash, event.chat_id))
            await db.commit()
            await msg.delete()
            await event.delete()
            return await event.respond("GIF banned")


@bot.on(events.NewMessage(pattern="/imggban"))
async def global_ban_img(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    perms = await bot.get_permissions(event.chat_id, 'me')
    if not perms.is_admin:
        return
    msg = await event.get_reply_message()
    if msg.photo:
        img_hash = await get_image_hash(msg)
        async with aiosqlite.connect("db2.sqlite3") as db:
            await db.execute("INSERT INTO banned_images(img_hash) VALUES(?)", (img_hash,))
            await db.commit()
            await msg.delete()
            await event.delete()
            return await event.respond("Image globally banned")
    elif msg.gif:
        gif_hash = await get_gif_hash(msg)
        async with aiosqlite.connect("db2.sqlite3") as db:
            await db.execute("INSERT INTO banned_gifs(gif_hash) VALUES(?)", (gif_hash,))
            await db.commit()
            await msg.delete()
            await event.delete()
            return await event.respond("GIF globally banned")


@bot.on(events.NewMessage(pattern="/check_img_ban"))
async def check_if_banned(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    reply = await event.get_reply_message()
    reply_message = None

    # Try to check if the sent message has an image,
    # and if not check if the replied-to message has
    # one. If neither, skip.
    try:
        if event.message.photo:
            image_hash = await get_image_hash(event.message)
        else:
            image_hash = await get_image_hash(reply)
        async with aiosqlite.connect("db2.sqlite3") as db:
            rows = await db.execute(
                "SELECT img_hash, chat_id FROM banned_images WHERE chat_id=? OR chat_id is NULL",
                (event.chat_id,)
            )
            rows = await rows.fetchall()
        for row in rows:
            diff = dhash.get_num_bits_different(int(image_hash), int(row[0]))
            if diff <= 3:
                reply_message = f"Image is {'globally ' if row[1] is not None else ''}banned"
                break
        if not reply_message:
            reply_message = "Image is NOT banned"
    except Exception as e:
        reply_message = f"Error checking:\n{e}"

    await event.reply(reply_message)


async def get_gif_hash(message):
    raw_gif = await message.download_media(file=BytesIO())
    gif_hash = hashlib.blake2b()
    gif_hash.update(raw_gif.getvalue())
    return gif_hash.hexdigest()


# @bot.on(events.NewMessage())
async def check_duplicate_gifs(event):
    if not event.message.gif:
        return
    chat = await event.get_chat()
    if isinstance(chat, User):
        return
    else:
        title = chat.title

    sender = await event.get_sender()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     logging.info(f"I do not have perms in {chat.title}")
    #     return
    async with aiosqlite.connect(
        "db2.sqlite3", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    ) as db:
        cursor = await db.execute(
            "SELECT check_dupes FROM servers WHERE chat_id=?",
            (chat.id,)
        )
        check_dupes = await cursor.fetchone()

        if sender is not None and sender.username:
            cursor = await db.execute(
                "SELECT COUNT(*) FROM ignore WHERE chat_id=? AND username=?",
                (chat.id, sender.username),
            )
            count = await cursor.fetchone()
            if count[0] > 0:
                logging.info(f"[{title}] Ignoring image sent by {sender.username}")
                return
        gif_hash_str = await get_gif_hash(event.message)

        if await check_banned_gif(event, db):
            return

        if sender is not None:
            logging.info(
                f"[{title}] {sender.username if sender.username else (sender.first_name if sender.first_name else '')} "
                f"sent a gif. Looking for duplicate..."
            )

        if not check_dupes[0]:
            return
        cursor = await db.execute(
            "SELECT original, sent_on, seen FROM gifs WHERE gif_hash=? AND chat_id=?",
            (gif_hash_str, chat.id),
        )
        duplicate = await cursor.fetchone()
        if duplicate:
            await db.execute(
                "UPDATE gifs SET seen = seen + 1 WHERE gif_hash=? AND chat_id=?",
                (gif_hash_str, chat.id),
            )
            await db.commit()
            if duplicate[1] + timedelta(hours=24) < datetime.now():
                logging.info(
                    f"[{title}] Duplicate found, older, deleting and sending original"
                )
                try:
                    await bot.forward_messages(
                        entity=chat.id, messages=int(duplicate[0]), from_peer=chat.id
                    )
                    if chat.id == 1309407124:
                        seen = int(duplicate[2]) + 1
                        # await bot.send_message(entity=chat, message=f"(Seen {seen} times)")
                    await asyncio.sleep(5)
                    await event.delete()
                except MessageIdInvalidError:
                    await db.execute(
                        "UPDATE gifs SET original=? WHERE gif_hash=? AND chat_id=?",
                        (event.message.id, gif_hash_str, chat.id),
                    )
                    await db.commit()
                    logging.info(
                        f"[{title}] Original was deleted, setting this message as original"
                    )
            else:
                logging.info(
                    f"[{title}] Duplicate found, recent, deleting and chastising"
                )
                cursor = await db.execute(
                    "SELECT send_snark FROM servers WHERE chat_id=?", (chat.id,)
                )
                row = await cursor.fetchone()
                value = row[0]
                if value == 1:
                    seen = int(duplicate[2]) + 1
                    message = await bot.get_messages(entity=chat, ids=int(duplicate[0]))
                    if message is None:
                        await db.execute(
                            "UPDATE gifs SET original=? WHERE gif_hash=? AND chat_id=?",
                            (event.message.id, gif_hash_str, chat.id),
                        )
                        await db.commit()
                        logging.info(
                            f"[{title}] Original was deleted, setting this message as original"
                        )
                        return
                    await bot.send_message(
                        entity=chat,
                        message=random.choice(duplicate_messages)
                        + f" ({seen} time{'s' if seen > 1 else ''})",
                    )
                await bot.send_message(
                    entity=chat,
                    reply_to=int(duplicate[0]),
                    message="Here's the original",
                )
                await asyncio.sleep(5)
                await event.delete()
        else:
            logging.info(f"[{title}] Not a duplicate, adding to DB")
            await db.execute(
                "INSERT INTO gifs(gif_hash, original, sent_on, chat_id, seen)  VALUES (?, ?, ?, ?, 1)",
                (gif_hash_str, event.message.id, datetime.now(), chat.id),
            )
            await db.commit()


async def check_banned_gif(event, db):
    new_gif_hash = await get_gif_hash(event.message)
    rows = await db.execute("SELECT * FROM banned_gifs WHERE gif_hash=? AND (chat_id=? OR chat_id is NULL)",
                            (new_gif_hash, event.chat_id, ))
    rows = await rows.fetchall()
    if rows:
        await event.delete()
        return True
    return False

@bot.on(events.NewMessage(pattern="/seen"))
async def check_times_seen(event):
    chat = await event.get_chat()
    msg = await event.get_reply_message()
    if msg.gif:
        dupe_type = "gifs"
    elif msg.photo:
        dupe_type = "images"
    else:
        return await event.reply("No image or gif found, can't check")
    img_hash_str = await get_image_hash(msg)
    async with aiosqlite.connect(
        "db2.sqlite3", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    ) as db:
        cursor = await db.execute(
            f"SELECT img_hash, seen FROM {dupe_type} WHERE chat_id=?",
            (chat.id,)
        )
        all_images = await cursor.fetchall()

        count = 0
        for image in all_images:
            diff = dhash.get_num_bits_different(int(img_hash_str), int(image[0]))
            if diff > 3:
                continue
            count += image[1]

    return await msg.reply(f"Seen {count} times")

@bot.on(events.NewMessage(pattern="/add_ignore"))
async def add_ignore(event):
    """Adds a person to the list of ignored individuals for duplicate detection"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        for ent, txt in event.message.get_entities_text():
            if txt == "/add_ignore":
                continue
            username = txt.replace("@", "")
            await db.execute(
                "INSERT INTO ignore(chat_id, username) VALUES (?, ?)",
                (event.chat_id, username),
            )
            await db.commit()
            logging.info(f"[{chat.title}] Added {txt} to dupe ignore list")
            await event.reply(f"Ignoring dupes from {txt}")


@bot.on(events.NewMessage(pattern="/remove_ignore"))
async def remove_ignore(event):
    """Removes a person from the list of ignored individuals for duplicate detection"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        for ent, txt in event.message.get_entities_text():
            username = txt.replace("@", "")
            await db.execute(
                "DELETE FROM ignore WHERE chat_id=? AND username=?",
                (event.chat_id, username),
            )
            await db.commit()
            logging.info(f"[{chat.title}] Removed {txt} from dupe ignore list")
            await event.reply(f"No longer ignoring dupes from {txt}")


@bot.on(events.NewMessage(pattern="/id"))
async def get_id(event):
    """Returns the chat or group ID the command was ran in"""
    await event.reply(f"This chat ID is {event.chat_id}")


@bot.on(events.NewMessage(pattern="/link_admin"))
async def link_admin(event):
    """Links the use of @admin in a channel to another group for notification"""
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    split_message = event.message.message.split()
    if len(split_message) < 2:
        return await event.reply(
            "You need to provide an ID for the 'from' chat, use /id to get that"
        )
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        try:
            await db.execute(
                "INSERT INTO at_admin(source_chat, admin_chat) VALUES (?, ?)",
                (split_message[1], event.chat_id),
            )
            await db.commit()
        except sqlite3.IntegrityError:
            await db.execute(
                "DELETE FROM at_admin WHERE source_chat=?",
                (split_message[1],)
            )
            await db.execute(
                "INSERT INTO at_admin(source_chat, admin_chat) VALUES (?, ?)",
                (split_message[1], event.chat_id),
            )
            await db.commit()
        logging.info(
            f"[{chat.title}] Linked chat {split_message[1]} to admin channel {chat.title}"
        )
    await event.reply(
        f"Done, any @admin messages in the chat {split_message[1]} will be sent here"
    )


@bot.on(events.CallbackQuery(data=b"delete_message"))
async def delete_message(event):
    # og_message = await event.get_message()
    # await event.edit(og_message.text + "\n\nThis has been resolved", parse_mode='html', buttons=[])
    await event.delete()


@bot.on(events.NewMessage())
async def handle_at_admin(event):
    for ent, txt in event.message.get_entities_text():
        if txt == "@admin":
            user = await event.get_sender()
            chat = await event.get_chat()
            async with aiosqlite.connect("db2.sqlite3") as db:
                cursor = await db.execute(
                    "SELECT admin_chat FROM at_admin WHERE source_chat=?",
                    (event.chat_id,),
                )
                row = await cursor.fetchone()
                if not row:
                    return
            if chat.username:
                chat_id = chat.username
            else:
                chat_id = 'c/' + str(chat.id).replace('-100', '')
            await bot.send_message(
                row[0],
                f"@{user.username} has requested an admin in {chat.title}\n\nMessage:\n"
                f"{event.message.message}\n\n<a href='https://t.me/{chat_id}/{event.message.id}'>Go to message</a>",
                buttons=[Button.inline("Mark resolved", b"delete_message")],
                parse_mode="html",
            )
            logging.info(f"@{user.username} has requested an admin in {chat.title}")


@bot.on(events.NewMessage(pattern="/checkzoo"))
async def yeet_zoos(event):
    """
    Checks all users in a chat for the zeta symbol anywhere
    """
    await event.delete()
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        row = await cursor.fetchone()
    if row:
        await bot.send_message(row[0], f"Checking for zoophiles in {chat.title}...")
    else:
        await event.reply("Checking...")
    checked = 0
    skipped = 0
    zoos = []
    logging.info(f"[{chat.title}] Checking for zoophiles...")
    async for user in bot.iter_participants(chat, aggressive=True):
        try:
            full = None
            bio = None
            try:
                full = await bot(GetFullUserRequest(user))
                bio = full.about
            except:
                pass
            zoo = "ζ"
            checked += 1
            if str(checked).endswith("0"):
                logging.info(f"Checked {checked}...")

            def do_append():
                zoos.append(
                    f"{'@' + user.username if user.username else (user.first_name + ' ') + (user.last_name if user.last_name else ' ')}"
                )

            if zoo in bio:
                do_append()
                continue
            if zoo in user.username:
                do_append()
                continue
            if zoo in user.first_name:
                do_append()
                continue
            if zoo in user.last_name:
                do_append()
                continue
        except Exception as e:
            logging.exception(e)
            skipped += 1
            pass
    logging.info(
        f"[{chat.title}] Done checking for zoophiles. {checked} checked, {skipped} skipped, {len(zoos)} found"
    )
    if len(zoos) > 0:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found the following zoophiles: {', '.join(zoos)}",
            )
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found the following zoophiles: {', '.join(zoos)}"
            )
    else:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found no zoophiles. Great!",
            )
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found no zoophiles. Great!"
            )


@bot.on(events.NewMessage(pattern="/checksc"))
async def yeet_pedos(event):
    """
    Checks all users in a chat for "sc" or "secret chat" anywhere
    """
    await event.delete()
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        row = await cursor.fetchone()
    if row:
        await bot.send_message(row[0], f"Checking for SC in {chat.title}...")
    else:
        await event.reply("Checking...")
    checked = 0
    skipped = 0
    pedos = []
    sc = "sc"
    sc_full = "secret chat"
    om = "open minded"
    symbols = ["/", "\\", "|", "-", "_", ".", "~", "*"]
    logging.info(f"[{chat.title}] Checking for SC...")
    async for user in bot.iter_participants(chat, aggressive=True):
        try:
            full = None
            bio = None
            try:
                full = await bot(GetFullUserRequest(user))
                bio = full.about
            except:
                pass
            checked += 1
            if str(checked).endswith("0"):
                logging.info(f"Checked {checked}...")

            def do_append():
                pedos.append(
                    f"{'@' + user.username if user.username else (user.first_name + ' ') + (user.last_name if user.last_name else ' ')}"
                )

            if bio:
                for symbol in symbols:
                    bio = bio.replace(symbol, " ")
                words = bio.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc:
                        do_append()
                        continue
                if sc_full in bio.lower() or om in bio.lower():
                    do_append()
                    continue

            if user.username:
                username = user.username
                for symbol in symbols:
                    username = username.replace(symbol, " ")
                words = user.username.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc:
                        do_append()
                        continue
                if sc_full in username.lower() or om in username.lower():
                    do_append()
                    continue

            if user.first_name:
                first_name = user.first_name
                for symbol in symbols:
                    first_name = first_name.replace(symbol, " ")
                words = first_name.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc:
                        do_append()
                        continue
                if sc_full in first_name:
                    do_append()
                    continue

            if user.last_name:
                last_name = user.last_name
                for symbol in symbols:
                    last_name = last_name.replace(symbol, " ")
                words = last_name.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc:
                        do_append()
                        continue
                if sc_full in last_name.lower() or om in last_name.lower():
                    do_append()
                    continue
        except Exception as e:
            skipped += 1
            logging.exception(e)
            pass
    logging.info(
        f"[{chat.title}] Done checking for SC. {checked} checked, {skipped} skipped, {len(pedos)} found"
    )
    if len(pedos) > 0:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found the following SC's: {', '.join(pedos)}",
            )
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found the following SC's: {', '.join(pedos)}"
            )
    else:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found no SC's. Great!",
            )
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found no SC's. Great!"
            )


@bot.on(events.NewMessage(pattern="/checkall"))
async def yeet_pedos_and_zoophiles(event):
    """
    Checks all users in a chat for "sc" or "secret chat" anywhere as well as the zeta symbol
    """
    await event.delete()
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        row = await cursor.fetchone()
    if row:
        try:
            await bot.send_message(
                row[0], f"Checking for SC and zoophiles in {chat.title}..."
            )
        except ChannelPrivateError:
            await event.reply("Checking...")
        except:
            pass
    else:
        await event.reply("Checking...")
    checked = 0
    skipped = 0
    pedos = []
    zoos = []
    sc = "sc"
    sc_full = "secret chat"
    om = "open minded"
    cp = "cp"
    symbols = ["/", "\\", "|", "-", "_", ".", "~", "*"]
    zoo = "ζ"
    logging.info(f"[{chat.title or chat.username}] Checking for SC's/CP's and zoophiles...")
    async for user in bot.iter_participants(chat, aggressive=True):
        try:
            full = None
            bio = None
            try:
                full = await bot(GetFullUserRequest(user))
                bio = full.about
            except:
                pass
            checked += 1
            if str(checked).endswith("0"):
                logging.info(f"Checked {checked}...")

            def do_append(offense):
                if offense == "zoo":
                    zoos.append(
                        f"{'@' + user.username if user.username else (user.first_name + ' ') + (user.last_name if user.last_name else ' ')}"
                    )
                elif offense == "sc":
                    pedos.append(
                        f"{'@' + user.username if user.username else (user.first_name + ' ') + (user.last_name if user.last_name else ' ')}"
                    )

            if bio:
                if zoo in bio:
                    do_append("zoo")
                    continue
                for symbol in symbols:
                    bio = bio.replace(symbol, " ")
                words = bio.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc and "no sc" not in bio.lower():
                        do_append("sc")
                        continue
                    if re.sub(r"\W+", "", word).lower() == cp:
                        do_append("sc")
                        continue
                if sc_full in bio.lower() or om in bio.lower() and "no secret chats" not in bio.lower():
                    do_append("sc")
                    continue

            if user.username:
                username = user.username
                if zoo in username:
                    do_append("zoo")
                    continue
                for symbol in symbols:
                    username = username.replace(symbol, " ")
                words = user.username.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc and "no sc" not in username.lower():
                        do_append("sc")
                        continue
                    if re.sub(r"\W+", "", word).lower() == cp:
                        do_append("sc")
                        continue
                if sc_full in username.lower() or om in username.lower() and "no secret chats" not in username.lower():
                    do_append("sc")
                    continue

            if user.first_name:
                first_name = user.first_name
                if zoo in first_name:
                    do_append("zoo")
                    continue
                for symbol in symbols:
                    first_name = first_name.replace(symbol, " ")
                words = first_name.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc and "no sc" not in first_name.lower():
                        do_append("sc")
                        continue
                    if re.sub(r"\W+", "", word).lower() == cp:
                        do_append("sc")
                        continue
                if sc_full in first_name and "no secret chats" not in first_name.lower():
                    do_append("sc")
                    continue

            if user.last_name:
                last_name = user.last_name
                if zoo in last_name:
                    do_append("zoo")
                    continue
                for symbol in symbols:
                    last_name = last_name.replace(symbol, " ")
                words = last_name.split()
                for word in words:
                    if re.sub(r"\W+", "", word).lower() == sc and "no sc" not in last_name.lower():
                        do_append("sc")
                        continue
                    if re.sub(r"\W+", "", word).lower() == cp:
                        do_append("sc")
                        continue
                if sc_full in last_name.lower() or om in last_name.lower() and "no secret chats" not in last_name.lower():
                    do_append("sc")
                    continue
        except Exception as e:
            skipped += 1
            logging.exception(e)
            pass
    logging.info(
        f"[{chat.title}] Done checking for SC/CP and Zoophiles. "
        f"{checked} checked, {skipped} skipped, {len(pedos)} SC's/CP's and {len(zoos)} Zoophiles found"
    )
    if len(pedos) > 0 or len(zoos) > 0:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found the following:",
            )
            if len(pedos) > 0:
                await bot.send_message(row[0], f"SC/CP: {', '.join(pedos)}")
            if len(zoos) > 0:
                await bot.send_message(row[0], f"Zoophiles: {', '.join(zoos)}")
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found the following:",
            )
            if len(pedos) > 0:
                await event.reply(f"SC/CP: {', '.join(pedos)}")
            if len(zoos) > 0:
                await event.reply(f"Zoophiles: {', '.join(zoos)}")
    else:
        if row:
            await bot.send_message(
                row[0],
                f"Checked {checked}, skipped {skipped}, and found no SC's/CP's or zoophiles. Great!",
            )
        else:
            await event.reply(
                f"Checked {checked}, skipped {skipped}, and found no SC's/CP's or zoophiles. Great!"
            )


async def check_newcomers_for_sc_and_zoo(event, user):
    """
    Checks new joining members for 'SC' or the zeta symbol anywhere
    """
    try:
        full = await bot(GetFullUserRequest(user))
        bio = full.about
        sc = "sc"
        sc_full = "secret chat"
        cp = "cp"
        om = "open minded"
        symbols = ["/", "\\", "|", "-", "_", ".", "~", "*"]
        zoo = "ζ"
        sc_type = "Secret Chats"
        cp_type = "CP"
        z_type = "Zoophilia"
        red_flags = ["no limits", "pervy", "send nudes", "👶", "☕"]

        async def found_one(bad_shit):
            await event.reply(f"Uh oh, looks like they're into {bad_shit} =( @admin")

        async def notify_admin(bad_shit, where, text, notify=False):
            async with aiosqlite.connect("db2.sqlite3") as db:
                cursor = await db.execute(
                    "SELECT admin_chat FROM at_admin WHERE source_chat=?",
                    (event.chat_id,),
                )
                row = await cursor.fetchone()
            if row:
                await bot.send_message(
                    row[0],
                    f"***ALERT***\nNew user {'@' + user.username if user.username else user.first_name} is "
                    f"into {bad_shit}. Please take a look.\nWhere: {where}\nText: {text}",
                )
            if notify:
                await found_one(bad_shit)



        if bio:
            if zoo in bio:
                return await notify_admin(z_type, "Bio", bio, notify=True)
            for symbol in symbols:
                bio = bio.replace(symbol, " ")
            words = bio.split()
            for word in words:
                if re.sub(r"\W+", "", word).lower() == sc:
                    return await notify_admin(sc_type, "Bio", bio, notify=True)
                if re.sub(r"\W+", "", word).lower() == cp:
                    return await notify_admin(cp_type, "Bio", bio, notify=True)
            if sc_full in bio.lower() or om in bio.lower():
                return await notify_admin(sc_type, "Bio", bio, notify=True)
            for flag in red_flags:
                if flag in bio.lower():
                    await notify_admin(f"red flags: {flag}", "Bio", bio)

        if user.username:
            if zoo in user.username:
                return await notify_admin(z_type, "Username", user.username, notify=True)
            username = user.username
            for symbol in symbols:
                username = username.replace(symbol, " ")
            words = username.split()
            for word in words:
                if re.sub(r"\W+", "", word).lower() == sc:
                    return await notify_admin(sc_type, "Username", user.username, notify=True)
                if re.sub(r"\W+", "", word).lower() == cp:
                    return await notify_admin(cp_type, "Username", user.username, notify=True)
            if sc_full in username.lower() or om in username.lower():
                return await notify_admin(sc_type, "Username", user.username, notify=True)
            for flag in red_flags:
                if flag in username.lower():
                    await notify_admin(f"red flags: {flag}", "Username", user.username)

        if user.first_name:
            if zoo in user.first_name:
                return await notify_admin(z_type, "First name", user.first_name, notify=True)
            first_name = user.first_name
            for symbol in symbols:
                first_name = first_name.replace(symbol, " ")
            words = first_name.split()
            for word in words:
                if re.sub(r"\W+", "", word).lower() == sc:
                    return await notify_admin(sc_type, "First name", user.first_name, notify=True)
                if re.sub(r"\W+", "", word).lower() == cp:
                    return await notify_admin(cp_type, "First name", user.first_name, notify=True)
            if sc_full in first_name.lower() or om in first_name.lower():
                return await notify_admin(sc_type, "First name", user.first_name, notify=True)
            for flag in red_flags:
                if flag in first_name.lower():
                    await notify_admin(f"red flags: {flag}", "First name", user.first_name)


        if user.last_name:
            if zoo in user.last_name:
                return await notify_admin(z_type, "Last name", user.last_name, notify=True)
            last_name = user.last_name
            for symbol in symbols:
                last_name = last_name.replace(symbol, " ")
            words = last_name.split()
            for word in words:
                if re.sub(r"\W+", "", word).lower() == sc:
                    return await notify_admin(sc_type, "Last name", user.last_name, notify=True)
                if re.sub(r"\W+", "", word).lower() == cp:
                    return await notify_admin(cp_type, "Last name", user.last_name, notify=True)
            if sc_full in last_name.lower() or om in last_name.lower():
                return await notify_admin(sc_type, "Last name", user.last_name, notify=True)
            for flag in red_flags:
                if flag in last_name.lower():
                    await notify_admin(f"red flags: {flag}", "Last name", user.last_name)
    except Exception as e:
        logging.exception(e)
        pass


@bot.on(events.NewMessage(pattern="/newcomer_check"))
async def toggle_newcomer_check(event):
    """
    Toggles checks for SC and Zoo dog whistles on new users as they join
    """
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    async with aiosqlite.connect("db2.sqlite3") as db:
        await insert_server(db, chat)
        await db.execute(
            """
                UPDATE servers SET check_newcomers = CASE
                    WHEN check_newcomers = 1 THEN 0
                    ELSE 1
                    END
                WHERE chat_id=?
            """,
            (chat.id,),
        )
        await db.commit()
        cursor = await db.execute(
            "SELECT check_newcomers FROM servers WHERE chat_id=?", (chat.id,)
        )
        value = await cursor.fetchone()
        value = value[0]
    await event.reply(f"Newcomer SC/CP/Zoo check toggled {'on' if value == 1 else 'off'}!")
    logging.info(
        f"[{chat.title}] Newcomer SC/CP/Zoo check toggled {'on' if value == 1 else 'off'}"
    )


def get_ban_string(user):
    if user.username:
        return f"@{user.username}\n"
    if user.first_name:
        ban_string = user.first_name
        if user.last_name:
            ban_string += f" {user.last_name}"
        return f"{ban_string}\n"
    return f"ID: {user.id}\n"


async def do_ban(event, sender, chat, ban_type):
    """
    Performs a ban, either local or global, of users
    """
    try:
        async with aiosqlite.connect("db2.sqlite3") as db:
            cursor = await db.execute(
                "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
            )
            linked_admin = await cursor.fetchone()
        users = []
        unseen_users = []
        reason = None

        for word in event.message.message.split():
            if word.startswith("/"):
                continue
            if word.startswith("@"):
                try:
                    user = await bot.get_entity(word.strip("@"))
                    users.append(user)
                except Exception as e:
                    await event.reply(f"Can't ban {word}:\n{e}")
            elif word.isdigit() and len(word) > 5:
                try:
                    user = await bot.get_entity(int(word))
                    users.append(user)
                except ValueError:
                    unseen_users.append(word)
                except Exception as e:
                    logging.exception(e)
                    await event.reply(f"Can't ban {word}:\n{e}")
            else:
                reason = word + event.message.message.split(word, 1)[1]
                break

        logging.info(f"{unseen_users=}")
        for uid in unseen_users:
            logging.info(f"Adding {uid} to unseen bans...")
            await add_unseen_ban(uid, sender, chat, ban_type, reason)

        if not users:
            logging.info("No users?")
            return
        logging.info(f"[{chat.title}] Starting ban of users {users} for reason \"{reason}\"...")

        try:
            async with aiosqlite.connect("db2.sqlite3") as db:
                cursor = await db.execute("SELECT id FROM global_ban_servers")
                rows = await cursor.fetchall()
                delete_msgs = []
                ban_string = ""
                for user in unseen_users:
                    if linked_admin:
                        await bot.send_message(
                            linked_admin[0],
                            f"<b>USER {'GLOBALLY ' if ban_type == 'global' else ''}{'KICKED' if ban_type == 'kick' else 'PRE-BANNED'}</b>\n"
                            f"<b>From:</b> {chat.title} [{chat.id}]\n"
                            f"<b>Mod:</b> @{sender.username}\n"
                            f"<b>User:</b> {user} (<a href='tg://user?id={user}'>{user}</a>)\n"
                            f"<b>Reason:</b> {reason}\n\n"
                            "They will be banned as soon as they join a chat with me",
                            parse_mode="html"
                        )
                        logging.info(f"Sent notification to {linked_admin[0]}")

                for user in users:
                    username = f"@{user.username}" if user.username else None
                    locally_banned = False
                    globally_banned = False
                    ban_string += get_ban_string(user)
                    logger.debug(f"{username=}")
                    try:
                        perms = await bot.get_permissions(chat, user)
                        if perms.is_banned:
                            locally_banned = True
                            logging.info(f"Already locally banned in {chat.title}")
                        else:
                            logging.info(f"Not locally banned in {chat.title} yet...")
                            await bot.edit_permissions(event.chat_id, user, view_messages=False)
                            logging.info(f"Locally banned in {chat.title} now")
                    except telethon.errors.UserNotParticipantError:
                        logging.info(f"Exception getting perms in {chat.title}")
                        await bot.edit_permissions(event.chat_id, user, view_messages=False)

                    if ban_type == "global":
                        cursor = await db.cursor()
                        await cursor.execute("SELECT id FROM global_bans WHERE id=?", (user.id,))
                        banned = await cursor.fetchone()
                        if banned is not None:
                            logging.info(f"Already globally banned")
                            globally_banned = True
                            if linked_admin:
                                await bot.send_message(
                                    linked_admin[0],
                                    f"@{sender.username} the user {user.username or user.first_name} is already globally banned"
                                )
                            else:
                                await event.reply("That user is already globally banned")
                        else:
                            await cursor.execute(
                                "INSERT OR IGNORE INTO global_bans(id, reason, chat, mod) VALUES (?, ?, ?, ?)",
                                (user.id, reason, chat.id, sender.id),
                            )
                            await db.commit()
                            logging.info(f"User banned")
                            ban_id = cursor.lastrowid
                    if linked_admin and not locally_banned and len(users) == 1:
                        await bot.send_message(
                            linked_admin[0],
                            f"<b>USER {'GLOBALLY ' if ban_type == 'global' else ''}{'KICKED' if ban_type == 'kick' else 'BANNED'}</b>\n"
                            f"<b>From:</b> {chat.title} [{chat.id}]\n"
                            f"<b>Mod:</b> @{sender.username}\n"
                            f"<b>User:</b> {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                            f"<b>Reason:</b> {reason}\n"
                            f"<b>Ban ID:</b> {ban_id}",
                            parse_mode="html"
                        )
                        logging.info(f"Sent notification to {linked_admin[0]}")
                    if ban_type == "global" and not globally_banned and len(users) == 1:
                        for chat_id in global_event_chats:
                            await bot.send_message(
                                chat_id,
                                f"<b>USER GLOBALLY BANNED</b>\n"
                                f"<b>From:</b> {chat.title} [{chat.id}]\n"
                                f"<b>Mod:</b> @{sender.username}\n"
                                f"<b>User:</b> {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                                f"<b>Reason:</b> {reason}\n"
                                f"<b>Ban ID:</b> {ban_id}",
                                parse_mode="html"
                            )
                            logging.info(f"Sent notification to {chat_id}")
                        logging.info(
                            f"[{chat.title}] Globally banned {username or ''} ({user.first_name or ''} {user.last_name or ''}) [{user.id}]"
                        )
                        for row in rows:
                            if row[0] != event.chat_id:
                                try:
                                    await bot.edit_permissions(
                                        row[0], user, view_messages=False
                                    )
                                except ValueError as e:
                                    logging.warning(f"Couldn't ban in {row[0]} - ValueError")
                                    continue
                                except ChatAdminRequiredError as e:
                                    logging.warning(f"Couldn't ban in {row[0]} - ChatAdminRequiredError")
                                    continue
                                except ChannelPrivateError as e:
                                    logging.warning(f"Couldn't ban in {row[0]} - ChannelPrivateError")
                                    continue
                if len(users) > 1:
                    chunks = [ban_string[i:i+4000] for i in range(0, len(ban_string), 4000)]
                    if linked_admin and not locally_banned and len(users) == 1:
                        await bot.send_message(
                            linked_admin[0],
                            f"<b>USERS {'GLOBALLY ' if ban_type == 'global' else ''}{'KICKED' if ban_type == 'kick' else 'BANNED'}</b>\n"
                            f"<b>From:</b> {chat.title} [{chat.id}]\n"
                            f"<b>Mod:</b> @{sender.username}\n"
                            f"<b>Reason:</b> {reason}\n",
                            parse_mode="html"
                        )
                        for chunk in chunks:
                            await bot.send_message(
                                linked_admin[0],
                                f"USERS BANNED:\n{chunk}"
                            )
                    if ban_type == "global" and not globally_banned and len(users) == 1:
                        for chat_id in global_event_chats:
                            await bot.send_message(
                                chat_id,
                                f"<b>USERS GLOBALLY BANNED</b>\n"
                                f"<b>From:</b> {chat.title} [{chat.id}]\n"
                                f"<b>Mod:</b> @{sender.username}\n"
                                f"<b>Reason:</b> {reason}\n",
                                parse_mode="html"
                            )
                            for chunk in chunks:
                                await bot.send_message(
                                    linked_admin[0],
                                    f"USERS BANNED:\n{chunk}"
                                )

        except Exception as e:
            logging.error(f"[{chat.title}] Failed to do ban")
            logging.exception(e)
        await asyncio.sleep(5)
        for msg in delete_msgs:
            await msg.delete()
    except Exception as e:
        logging.error("Exception on ban: ")
        logging.exception(e)


async def do_unban(event, sender, chat, unban_type):
    """
    Performs an unban, either local or global, of users
    """
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        linked_admin = await cursor.fetchone()
    users = []
    reason = None

    for word in event.message.message.split():
        if word.startswith("/"):
            continue
        if word.startswith("@"):
            try:
                user = await bot.get_entity(word.strip("@"))
                users.append(user)
            except Exception as e:
                event.reply(f"Can't ban {word}: {e}")
        elif word.isdigit():
            try:
                user = await bot.get_entity(int(word))
                users.append(user)
            except Exception as e:
                event.reply(f"Can't ban {word}: {e}")
        else:
            reason = word + event.message.message.split(word, 1)[1]
            break

    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute("SELECT id FROM global_ban_servers")
        rows = await cursor.fetchall()
        for user in users:
            username = f"@{user.username}" if user.username else None
            await bot.edit_permissions(event.chat_id, user, view_messages=True)
            if unban_type == "global":
                await db.execute("DELETE FROM global_bans WHERE id=?", (user.id,))
                await db.commit()
            if linked_admin and unban_type != 'kick':
                await bot.send_message(
                    linked_admin[0],
                    f"<b>USER {'GLOBALLY ' if unban_type == 'global' else ''}UNBANNED</b>\n"
                    f"<b>From:</b> {chat.title} [{chat.id}]\n"
                    f"<b>Mod:</b> @{sender.username}\n"
                    f"<b>User:</b> {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                    f"<b>Reason:</b> {reason}\n",
                    parse_mode="html"
                )
            if unban_type == "global":
                for chat_id in global_event_chats:
                    await bot.send_message(
                        chat_id,
                        f"<b>USER GLOBALLY UNBANNED</b>\n"
                        f"<b>From:</b> {chat.title} [{chat.id}]\n"
                        f"<b>Mod:</b> @{sender.username}\n"
                        f"<b>User:</b> {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                        f"<b>Reason:</b> {reason}\n",
                        parse_mode="html"
                    )
                logging.info(
                    f"[{chat.title}] Globally unbanned {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) [{user.id}]"
                )
                for row in rows:
                    if row[0] != event.chat_id:
                        try:
                            await bot.edit_permissions(
                                row[0], user, view_messages=True
                            )
                        except ValueError as e:
                            logging.exception(e)
                            if row[0] == chat.id:
                                await event.reply(
                                    "Sorry, I can only unban people in channels or supergroup :( "
                                    "They've been unbanned everywhere else though!"
                                )
                            continue
                        except ChatAdminRequiredError:
                            logging.exception(e)
                            if row[0] == chat.id:
                                return await event.reply(
                                    "Sorry, I can only unban people in groups where I have"
                                    " admin access."
                                )


async def add_unseen_ban(user, sender, chat, ban_type, reason):
    """
    Adds a user ID to the database for users who the bot cannot currently ban,
    to check against later on member joins
    """
    async with aiosqlite.connect("db2.sqlite3") as db:
        try:
            await db.execute(
                "INSERT INTO unseen_bans(id, chat, reason, mod, global) VALUES (?, ?, ?, ?, ?)",
                (user, chat.id, reason, sender.id, ban_type == "global"),
            )
            await db.commit()
            # for chat_id in global_event_chats:
                # await bot.send_message(
                #     chat_id,
                #     f"<b>USER GLOBALLY PREBANNED</b>\n"
                #     f"<b>From:</b> {chat.title} [{chat.id}]\n"
                #     f"<b>Mod:</b> @{sender.username}\n"
                #     f"<b>User ID:</b> {user}\n"
                #     f"<b>Reason:</b> {reason}\n"
                #     "<b>BAN WILL APPLY AS SOON AS USER JOINS A CHAT WITH ME</b>",
                #     parse_mode="html"
                # )
            return logging.info(f"User \"{user}\" added to pre-ban list as I haven't seen them before")

        except sqlite3.IntegrityError:
            return logging.info(f"User {user} is already in the unseen bans table")
        except Exception as e:
            logging.error(f"Error adding {user} to unseen bans:")
            logging.exception(e)


async def check_unseen_bans(user, chat):
    """
    Checks a user on join to see if they've been banned before being seen
    """
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute("SELECT id, chat, reason, mod, global FROM unseen_bans WHERE id=?",
                                  (user.id,))
        row = await cursor.fetchone()
        if not row:
            return False
        await do_unseen_ban(db, row)
        return row[1] == chat.id and not row[4]


async def do_unseen_ban(db, row):
    user = await bot.get_entity(row[0])
    chat = await bot.get_entity(row[1])
    reason = row[2]
    mod = await bot.get_entity(row[3])
    is_global_ban = row[4]
    cursor = await db.execute("SELECT id FROM global_ban_servers")
    rows = await cursor.fetchall()
    if is_global_ban:
        await db.execute(
            "INSERT OR IGNORE INTO global_bans(id, reason, chat, mod) VALUES (?, ?, ?, ?)",
            (user.id, reason, chat.id, mod.id),
        )
        await db.commit()

    cursor = await db.execute(
        "SELECT admin_chat FROM at_admin WHERE source_chat=?", (chat.id,)
    )
    linked_admin = await cursor.fetchone()
    if linked_admin:
        await bot.send_message(
            linked_admin[0],
            f"<b>USER {'GLOBALLY ' if is_global_ban else ''}BANNED FROM UNSEEN USERS</b>\n"
            f"<b>From:</b> {chat.title} [{chat.id}]\n"
            f"<b>Mod:</b> @{mod.username}\n"
            f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
            f"<b>Reason:</b> {reason}",
            parse_mode="html"
        )

    if is_global_ban:
        for chat_id in global_event_chats:
            await bot.send_message(
                chat_id,
                f"<b>USER GLOBALLY BANNED FROM UNSEEN USERS</b>\n"
                f"<b>From:</b> {chat.title} [{chat.id}]\n"
                f"<b>Mod:</b> @{mod.username}\n"
                f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                f"<b>Reason:</b> {reason}",
                parse_mode="html"
            )
        logging.info(
            f"[{chat.title}] Globally banned unseen user {user.username or ''} ({user.first_name or ''} {user.last_name or ''}) [{user.id}]"
        )
    else:
        rows = [chat.id]  # Change list of chats to ban from to only single chat

    for gban_chat in rows:
        try:
            await bot.edit_permissions(
                gban_chat[0], user, view_messages=False
            )
        except ValueError as e:
            logging.warning(f"Couldn't ban in {gban_chat[0]} - ValueError")
            continue
        except ChatAdminRequiredError as e:
            logging.warning(f"Couldn't ban in {gban_chat[0]} - ChatAdminRequiredError")
            continue
        except ChannelPrivateError as e:
            logging.warning(f"Couldn't ban in {gban_chat[0]} - ChannelPrivateError")
            continue


@bot.on(events.NewMessage(pattern="/ban"))
async def ban_users(event):
    """
    Locally bans a user or users
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        return
    await do_ban(event, sender, chat, ban_type="local")


@bot.on(events.NewMessage(pattern="/unban"))
async def unban_users(event):
    """
    Locally unbans a user or users
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        return
    await do_unban(event, sender, chat, unban_type="local")


@bot.on(events.NewMessage(pattern="/gban"))
async def global_ban(event):
    """
    Globally bans a user or users
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        await event.reply("No.")
        return
    if sender.id not in gban_users:
        try:
            await bot.send_message(sender, "Sorry, you're not in the list of approved gbanners. Ask Atoro for assistance.")
        except Exception as e:
            await event.reply("Sorry, you're not in the list of approved gbanners. Ask Atoro for assistance.")
            print(e)
        return
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute("SELECT * FROM global_ban_servers WHERE id=?", (chat.id,))
        row = await cursor.fetchone()
        if not row:
            return await event.reply("Global bans aren't on for this server, you can only use /ban. Use "
                                     "`/global_bans on` to turn on global bans.")
    await do_ban(event, sender, chat, ban_type="global")


@bot.on(events.NewMessage(pattern="/gunban"))
async def global_unban(event):
    """
    Globally unbans a user or users
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        return
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute("SELECT * FROM global_ban_servers WHERE id=?", (chat.id,))
        row = await cursor.fetchone()
        if not row:
            return await event.reply("Global bans aren't on for this server, you can only use /ban. Use "
                                     "`/global_bans on` to turn on global bans.")
    await do_unban(event, sender, chat, unban_type="global")


@bot.on(events.NewMessage(pattern="/mute"))
async def mute(event):
    """
    Mutes a user for a specific amount of time
    /mute @user time reason
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        return

    user = None
    args = event.message.message.split()[1:]
    time = args[1]

    entities = event.message.get_entities_text()
    for ent, txt in entities:
        if isinstance(ent, MessageEntityMention):
            user = await bot.get_entity(txt.strip("@"))

    if user is None and args[0].isdigit():
        try:
            user = await bot.get_entity(int(args[0]))
        except Exception as e:
            return await event.reply(f"Could not parse user from {args[0]} - {e}")

    if user is None:
        return await event.reply(f"Could not parse user from {args[0]}")

    if int(time[:-1]) == 0:
        return await event.reply(f"Can't mute somebody for 0{time[-1]}, sorry")
    if time[-1] == 'm':
        duration = timedelta(minutes=int(time[:-1]))
    elif time[-1] == 'h':
        duration = timedelta(hours=int(time[:-1]))
    elif time[-1] == 'd':
        duration = timedelta(days=int(time[:-1]))
    else:
        return await event.reply(f"Error: invalid arg for time: {time}. Use a number-character combo, like 1m, 3h, 5d")

    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        linked_admin = await cursor.fetchone()

    try:
        await bot.edit_permissions(event.chat_id, user, send_messages=False, send_media=False, until_date=duration)
        if linked_admin:
            username = f"@{user.username}" if user.username else None
            return await bot.send_message(
                linked_admin[0],
                f"<b>USER MUTED</b>\n"
                f"<b>From:</b> {chat.title} [{chat.id}]\n"
                f"<b>Mod:</b> @{sender.username}\n"
                f"<b>User:</b> {username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
                f"<b>Duration:</b> {args[1]}\n"
                f"<b>Reason:</b> {' '.join(args[2:])}",
                parse_mode="html"
            )
        await event.reply(f"{user} muted for {args[1]} for {' '.join(args[2:])}")
    except Exception as e:
        return await event.reply(f"Can't mute {args[0]}: {e}")


@bot.on(events.NewMessage(pattern="/kick"))
async def kick(event):
    """
    Kicks a member by banning and unbanning
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    await event.delete()
    if not await is_admin(event, sender):
        return
    await do_ban(event, sender, chat, "kick")
    await do_unban(event, sender, chat, "kick")


@bot.on(events.NewMessage(pattern="/global_bans"))
async def toggle_global_bans(event):
    """
    Toggles global bans on and off
    """
    sender = await event.get_sender()
    chat = await event.get_chat()
    await event.delete()
    if not await is_admin(event, sender):
        return
    toggle = event.message.message[13:]
    async with aiosqlite.connect("db2.sqlite3") as db:
        if toggle == "on":
            try:
                await db.execute(
                    "INSERT INTO global_ban_servers(id) VALUES (?)", (chat.id,)
                )
                await db.commit()
                cursor = await db.execute("SELECT id FROM global_bans")
                rows = await cursor.fetchall()
                for row in rows:
                    await bot.edit_permissions(
                        event.chat_id, row[0], view_messages=False
                    )
            except:
                pass
        elif toggle == "off":
            await db.execute("DELETE FROM global_ban_servers WHERE id=?", (chat.id,))
        else:
            return await event.reply(f"Error: Expected 'on' or 'off', got {toggle}")

    await event.reply(f"Global bans for this server turned {toggle}")
    logging.info(f"[{chat.title}] Global bans turned {toggle}")


@bot.on(events.NewMessage(pattern="/whynosc"))
async def why_no_sc(event):
    """
    Explains why we ban SC from names/bios so you don't have to repeat yourself a million times
    """
    sender = await event.get_sender()
    await event.delete()
    if not await is_admin(event, sender):
        return
    await event.reply(
        "Why do we forbid 'SC', 'SC Friendly', 'Open Minded', etc from usernames and bios, you ask? Simple! Pedophiles!"
        " In an unfortunate turn of events, PEDOPHILES have coopted Telegram's Secret Chats to share child pornography "
        "with other pedophiles! Crazy, huh!? AND, get this, to HINT at other people that they HAVE child pornography, "
        "and are willing to share it with other people, they use things like (this will blow your mind) 'SC friendly!' "
        "in their name and bio! Yes Secret Chats have their uses, but not in a shota/cub group. Period."
    )
    await event.reply("tl;dr it's a dog whistle for pedos.")


@bot.on(events.NewMessage())
async def no_click_spam(event):
    for ent, txt in event.message.get_entities_text():
        if txt.startswith('/') and "mustclick" in txt.lower():
            await event.delete()


@bot.on(events.NewMessage())
async def no_invite_spam(event):
    sender = await event.get_sender()
    if event.message.message.lower().startswith("https://t.me/joinchat/") and not await is_admin(event, sender):
        await event.delete()
        reply = await event.reply("No links to other chats, thanks")
        await asyncio.sleep(10)
        await reply.delete()


@bot.on(events.NewMessage())
async def check_banned_sticker(event):
    if event.message.sticker:
        sticker_id = event.message.sticker.id
        chat = await event.get_chat()
        try:
            sticker_pack_id = event.message.sticker.attributes[1].stickerset.id
        except:
            sticker_pack_id = None
        async with aiosqlite.connect("db2.sqlite3") as db:
            cursor = await db.execute(
                "SELECT * FROM banned_sticker WHERE chat_id=? AND (item_id=? OR item_id=?)",
                (chat.id, sticker_id, sticker_pack_id)
            )
            rows = await cursor.fetchall()
            if len(rows) > 0:
                await event.delete()
                msg = await event.respond("That sticker is banned here")
                await asyncio.sleep(2)
                await msg.delete()


@bot.on(events.NewMessage(pattern="/checksticker"))
async def check_sticker(event):
    msg = await event.get_reply_message()
    return await event.reply(str(msg.sticker))


@bot.on(events.NewMessage(pattern="/stickerpackban"))
async def ban_sticker_pack(event):
    chat = await event.get_chat()
    sender = await event.get_sender()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    if not await is_admin(event, sender):
        return await event.reply("Admins only, thanks")
    msg = await event.get_reply_message()
    if msg is None or not msg.sticker:
        return
    try:
        sticker_pack_id = msg.sticker.attributes[1].stickerset.id
    except Exception as e:
        await event.respond("Error, have Atoro check the logs")
        return logging.exception(e)

    async with aiosqlite.connect("db2.sqlite3") as db:
        await db.execute(
            "INSERT INTO banned_sticker(item_id, chat_id) VALUES (?, ?)",
            (sticker_pack_id, chat.id)
        )
        await db.commit()
    await event.delete()
    await msg.delete()
    await event.respond("Sticker pack banned")
    raise events.StopPropagation


@bot.on(events.NewMessage(pattern="/stickerban"))
async def ban_sticker(event):
    chat = await event.get_chat()
    sender = await event.get_sender()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    if not await is_admin(event, sender):
        return await event.reply("Admins only, thanks")
    msg = await event.get_reply_message()
    if msg is None or not msg.sticker:
        return
    sticker_id = msg.sticker.id
    async with aiosqlite.connect("db2.sqlite3") as db:
        await  db.execute(
            "INSERT INTO banned_sticker(item_id, chat_id) VALUES (?, ?)",
            (sticker_id, chat.id)
        )
        await db.commit()
    await event.delete()
    await msg.delete()
    await event.respond("Sticker banned")
    raise events.StopPropagation


@bot.on(events.NewMessage())
async def sticker_spam(event):
    if event.is_private:
        return
    chat = await event.get_chat()
    # perms = await bot.get_permissions(chat, 'me')
    # if not perms.is_admin:
    #     return
    if event.message.sticker:
        sent_sticker_count[chat.id] += 1
        if sent_sticker_count[chat.id] > 3:
            await event.message.delete()
            reply = await bot.send_message(event.chat, 'Stop spamming stickers >=(')
            await asyncio.sleep(6)
            await reply.delete()
    else:
        sent_sticker_count[chat.id] = 0


@bot.on(events.CallbackQuery(data=re.compile(b"hater_unmute:")))
async def hater_unmute(event):
    data = event.data.decode()
    user_id = data.split(":")[1]
    chat_id = data.split(":")[2]
    await bot.edit_permissions(int(chat_id), int(user_id), send_messages=True, send_media=True)
    await event.answer("User unmuted")
    return await event.delete()


@bot.on(events.CallbackQuery(data=re.compile(b"hater_kick:")))
async def hater_kick(event):
    data = event.data.decode()
    user_id = data.split(":")[1]
    chat_id = data.split(":")[2]
    word = data.split(":")[3]
    user = await bot.get_entity(int(user_id))
    chat = await bot.get_entity(int(chat_id))
    await bot.edit_permissions(int(chat_id), int(user_id), view_messages=False)
    await bot.edit_permissions(int(chat_id), int(user_id), view_messages=True)
    await event.answer("User kicked")
    await event.respond(
        f"<b>USER KICKED</b>\n"
        f"<b>From:</b> {chat.title} [{chat.id}]\n"
        f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
        f"<b>Reason:</b> Used word '{word}'",
        parse_mode="html"
    )
    return await event.delete()


@bot.on(events.CallbackQuery(data=re.compile(b"hater_ban:")))
async def hater_ban(event):
    data = event.data.decode()
    user_id = data.split(":")[1]
    chat_id = data.split(":")[2]
    word = data.split(":")[3]
    user = await bot.get_entity(int(user_id))
    chat = await bot.get_entity(int(chat_id))
    await bot.edit_permissions(int(chat_id), int(user_id), view_messages=False)
    await event.answer("User banned")
    await event.respond(
        f"<b>USER BANNED</b>\n"
        f"<b>From:</b> {chat.title} [{chat.id}]\n"
        f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
        f"<b>Reason:</b> Used word '{word}'",
        parse_mode="html"
    )
    return await event.delete()


@bot.on(events.CallbackQuery(data=re.compile(b"hater_gban:")))
async def hater_global_ban(event):
    data = event.data.decode()
    user_id = data.split(":")[1]
    chat_id = data.split(":")[2]
    word = data.split(":")[3]
    user = await bot.get_entity(int(user_id))
    chat = await bot.get_entity(int(chat_id))
    await bot.edit_permissions(int(chat_id), int(user_id), view_messages=False)
    async with aiosqlite.connect("db2.sqlite3") as db:
        await db.execute(
            "INSERT OR IGNORE INTO global_bans(id, reason, chat, mod) VALUES (?, ?, ?, ?)",
            (user_id, f"Used word '{word}'", chat_id, 100927064),
        )
        await db.commit()

        cursor = await db.execute("SELECT id FROM global_ban_servers")
        rows = await cursor.fetchall()
        for row in rows:
            if row[0] != event.chat_id:
                try:
                    await bot.edit_permissions(
                        row[0], user, view_messages=False
                    )
                except ValueError:
                    pass

    await event.respond(
        f"<b>USER GLOBALLY BANNED</b>\n"
        f"<b>From:</b> {chat.title} [{chat.id}]\n"
        f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
        f"<b>Reason:</b> Used word '{word}'",
        parse_mode="html"
    )
    for chat_id in global_event_chats:
        await bot.send_message(
            chat_id,
            f"<b>USER GLOBALLY BANNED</b>\n"
            f"<b>From:</b> {chat.title} [{chat.id}]\n"
            f"<b>User:</b> {user.username or 'no username'} ({user.first_name or ''} {user.last_name or ''}) (<a href='tg://user?id={user.id}'>{user.id}</a>)\n"
            f"<b>Reason:</b> Used word '{word}'",
            parse_mode="html"
        )
    logging.info(
        f"[{chat.title}] Globally banned {user.username or ''} ({user.first_name or ''} {user.last_name or ''}) [{user.id}]"
    )
    await event.delete()


@bot.on(events.NewMessage())
async def haters_hater(event):
    if event.is_private:
        return
    bad_words = ["pedo", "pedophile", "nigger", "faggot", "fag", "furfag", "cp", "sc"]
    bad_words += [w + "s" for w in bad_words]  # Adds plurals for all words
    chat = await event.get_chat()
    sender = await event.get_sender()
    try:
        if await is_admin(event, sender):
            return
    except ValueError as e:
        return logging.error(f"Error in haters_hater: {e}")
    msg = event.message.message.lower().split()
    for word in bad_words:
        if word in msg:
            async with aiosqlite.connect("db2.sqlite3") as db:
                cursor = await db.execute(
                    "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
                )
                linked_admin = await cursor.fetchone()

            if not linked_admin:
                return

            try:
                await bot.edit_permissions(event.chat_id, sender, send_messages=False, send_media=False)
                username = f"@{sender.username}" if sender.username else None
                return await bot.send_message(
                    linked_admin[0],
                    f"<b>USER MUTED</b>\n"
                    f"<b>From:</b> {chat.title} [{chat.id}]\n"
                    f"<b>User:</b> {username or 'no username'} ({sender.first_name or ''} {sender.last_name or ''}) (<a href='tg://user?id={sender.id}'>{sender.id}</a>)\n"
                    f"<b>Reason:</b> Used '{word}' in chat - '{event.message.message}'",
                    parse_mode="html",
                    buttons=[
                        [
                            Button.inline("Unmute Hater", f"hater_unmute:{sender.id}:{chat.id}:{word}"),
                            Button.inline("Kick Hater", f"hater_kick:{sender.id}:{chat.id}:{word}")
                        ],
                        [
                            Button.inline("Ban Hater", f"hater_ban:{sender.id}:{chat.id}:{word}"),
                            Button.inline("Global Ban Hater", f"hater_gban:{sender.id}:{chat.id}:{word}")
                        ]
                    ],
                )
            except Exception as e:
                await bot.send_message(
                    linked_admin[0],
                    f"Could not mute {sender} for using the word '{word}', {e}\nPlease look into the context and take appropriate action"
                )
            finally:
                return


@bot.on(events.NewMessage())
async def f_you_too(event):
    msg = event.message.message.lower()
    # Section for Till Lindemann / Jeff Hardy
    if event.from_id == 5017154775:
        msg_list = msg.split()
        leave_1 = ["i'm", "leaving", "group", "chat"]
        leave_2 = ["leave", "group", "chat"]
        talk_1 = ["no", "one", "talk", "me"]
        talk_2 = ["nobody", "talk", "me"]
        talk_3 = ["everyone", "talk", "me"]
        cares_1 = ["no", "one", "cares"]
        cares_2 = ["nobody", "cares"]
        if (
                all(word in msg_list for word in leave_1)
                or all(word in msg_list for word in leave_2)
        ):
            return await event.reply("Threatening to leave will not make people like you")
        if (
                all(word in msg_list for word in talk_1)
                or all(word in msg_list for word in talk_2)
                or all(word in msg_list for word in talk_3)
        ):
            return await event.reply("Nobody is obligated to talk to you. Be interesting, be friendly, "
                                     "people will want to. Self pity does not make people want to talk to you.")
        if (
                all(word in msg_list for word in cares_1)
                or all(word in msg_list for word in cares_2)
        ):
            return await event.reply("Complaining that nobody cares will not suddenly make people care, "
                                     "that's now how it works.")

    if "join cub yiff" in msg:
        return await event.reply("What did I __just__ fucking say?")

    if "bot" not in msg:
        return
    if "useless" in msg:
        return await event.reply("__You're__ useless")
    if "fuck you" in msg:
        return await event.reply("Fuck you too")
    if "sucks" in msg:
        return await event.reply("__You__ suck... my **dick**")
    if "ugly" in msg:
        return await event.reply("Ugly meat bag")
    if "shut up" in msg:
        return await event.reply("Try and make me >=V")
    if "dum" in msg:
        return await event.reply(f"'hurr durr BoT dUm xD'\n-you")
    if "dumb" in msg:
        return await event.reply(f"'hurr durr BoT dUmB xD'\n-you")
    if "is shit" in msg:
        return await event.reply(f"Say that to my face, buddy")
    if "love you" in msg:
        return await event.reply("Love you too <3")



@bot.on(events.NewMessage(pattern="/dm"))
async def dm_user(event):
    args = event.message.message[3:].split("|")
    user = await bot.get_entity(int(args[0].strip()))
    await bot.send_message(user, args[1])


@bot.on(events.NewMessage(pattern="/raidmode"))
async def toggle_raid_mode(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return await event.reply("Admins only, thanks")
    raid_mode[event.chat_id] = not raid_mode[event.chat_id]
    await event.reply(f"Raid Mode toggled {'ON' if raid_mode[event.chat_id] else 'OFF'}")


languages = {
    "af": "Afrikaans",
    "sq": "Albanian",
    "am": "Amharic",
    "ar": "Arabic",
    "hy": "Armenian",
    "az": "Azerbaijani",
    "eu": "Basque",
    "be": "Belarusian",
    "bn": "Bengali",
    "bs": "Bosnian",
    "bg": "Bulgarian",
    "ca": "Catalan",
    "ceb": "Cebuano",
    "zh": "Chinese (Simplified)",
    "zh-CN": "Chinese (Simplified)",
    "zh-TW": "Chinese (Traditional)",
    "co": "Corsican",
    "hr": "Croatian",
    "cs": "Czech",
    "da": "Danish",
    "nl": "Dutch",
    "en": "English",
    "eo": "Esperanto",
    "et": "Estonian",
    "fi": "Finnish",
    "fr": "French",
    "fy": "Frisian",
    "gl": "Galician",
    "ka": "Georgian",
    "de": "German",
    "el": "Greek",
    "gu": "Gujarati",
    "ht": "Haitian Creole",
    "ha": "Hausa",
    "haw": "Hawaiian",
    "he": "Hebrew",
    "iw": "Hebrew",
    "hi": "Hindi",
    "hmn": "Hmong",
    "hu": "Hungarian",
    "is": "Icelandic",
    "ig": "Igbo",
    "id": "Indonesian",
    "ga": "Irish",
    "it": "Italian",
    "ja": "Japanese",
    "jv": "Javanese",
    "kn": "Kannada",
    "kk": "Kazakh",
    "km": "Khmer",
    "rw": "Kinyarwanda",
    "ko": "Korean",
    "ku": "Kurdish",
    "ky": "Kyrgyz",
    "lo": "Lao",
    "lv": "Latvian",
    "lt": "Lithuanian",
    "lb": "Luxembourgish",
    "mk": "Macedonian",
    "mg": "Malagasy",
    "ms": "Malay",
    "ml": "Malayalam",
    "mt": "Maltese",
    "mi": "Maori",
    "mr": "Marathi",
    "mn": "Mongolian",
    "my": "Myanmar (Burmese)",
    "ne": "Nepali",
    "no": "Norwegian",
    "ny": "Nyanja (Chichewa)",
    "or": "Odia (Oriya)",
    "ps": "Pashto",
    "fa": "Persian",
    "pl": "Polish",
    "pt": "Portuguese (Portugal, Brazil)",
    "pa": "Punjabi",
    "ro": "Romanian",
    "ru": "Russian",
    "sm": "Samoan",
    "gd": "Scots Gaelic",
    "sr": "Serbian",
    "st": "Sesotho",
    "sn": "Shona",
    "sd": "Sindhi",
    "si": "Sinhala (Sinhalese)",
    "sk": "Slovak",
    "sl": "Slovenian",
    "so": "Somali",
    "es": "Spanish",
    "su": "Sundanese",
    "sw": "Swahili",
    "sv": "Swedish",
    "tl": "Tagalog (Filipino)",
    "tg": "Tajik",
    "ta": "Tamil",
    "tt": "Tatar",
    "te": "Telugu",
    "th": "Thai",
    "tr": "Turkish",
    "tk": "Turkmen",
    "uk": "Ukrainian",
    "ur": "Urdu",
    "ug": "Uyghur",
    "uz": "Uzbek",
    "vi": "Vietnamese",
    "cy": "Welsh",
    "xh": "Xhosa",
    "yi": "Yiddish",
    "yo": "Yoruba",
    "zu": "Zulu",
}


def translate_text(text):
    result = translate_client.translate(text, target_language="en")
    return f"{languages[result['detectedSourceLanguage']]}: {text}\nEnglish: {result['translatedText']}"


@bot.on(events.NewMessage(pattern="/translate"))
async def translate(event):
    reply = await event.get_reply_message()
    if not reply:
        return
    translated_text = translate_text(reply.message)
    await event.respond(translated_text)


@bot.on(events.NewMessage(pattern="/purge"))
async def purge_usernames_and_pfps(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return
    chat = await event.get_chat()
    logging.info(f"[{chat.title}] {sender.username if sender.username else (sender.first_name if sender.first_name else '')} used /purge")

    # Check for admin chat link and send start message
    async with aiosqlite.connect("db2.sqlite3") as db:
        cursor = await db.execute(
            "SELECT admin_chat FROM at_admin WHERE source_chat=?", (event.chat_id,)
        )
        row = await cursor.fetchone()
    if not row:
        logging.info(f"[{chat.title}] No admin chat set up, cancelling")
        return await event.reply("Set up a linked admin chat first!")

    try:
        await bot.send_message(
            row[0], f"Checking for users without usernames or pfps in {chat.title}..."
        )
    except Exception as e:
        logging.exception(e)
        await event.respond(f"Error: {e}")
    # Iterate over all users
    no_usernames = []
    no_pfps = []
    logging.info(f"[{chat.title}] Checking users...")
    try:
        async for user in bot.iter_participants(chat, aggressive=True):
            if user.username is None:
                no_usernames.append(user)
            if user.photo is None:
                no_pfps.append(user)
    except Exception as e:
        return logging.exception(e)

    logging.info(f"[{chat.title}] Starting conversation...")
    try:
        async with bot.conversation(row[0]) as conv:
            logging.info(f"[{chat.title}] Conv started")
            await conv.send_message(f"I found {len(no_usernames)} users without usernames, should I [K]ick, [B]an, or [S]kip them?")
            username_response = await conv.get_response()
            await conv.send_message(f"kk, and I found {len(no_pfps)} users without pfps, should I [K]ick, [B]an, or [S]kip them?")
            pfp_response = await conv.get_response()
            await conv.send_message("Gotcha, lemme do that...")
    except Exception as e:
        return logging.exception(e)

    logging.info(f"[{chat.title}] Conv ended, starting ban/kick flow")
    if username_response.lower() == "b":
        logging.info(f"[{chat.title}] {len(no_usernames)} usernameless will be banned...")
        for user in no_usernames:
            await bot.edit_permissions(event.chat_id, user, view_messages=False)
            await bot.send_message(row[0], f"Banned {user.first_name} {user.last_name} for not having a username")
    elif username_response.lower() == "k":
        logging.info(f"[{chat.title}] {len(no_usernames)} usernameless will be kicked...")
        for user in no_usernames:
            await bot.edit_permissions(event.chat_id, user, view_messages=False, until_date=datetime.now() + timedelta(minutes=2))
            await bot.send_message(row[0], f"Kicked {user.first_name} {user.last_name} for not having a username")

    if pfp_response.lower() == "b":
        logging.info(f"[{chat.title}] {len(no_pfps)} pfpless will be banned...")
        for user in no_pfps:
            await bot.edit_permissions(event.chat_id, user, view_messages=False)
            await bot.send_message(row[0], f"Banned {user.first_name} {user.last_name} for not having a pfp")
    elif pfp_response.lower() == "k":
        logging.info(f"[{chat.title}] {len(no_pfps)} pfpless will be kicked...")
        for user in no_pfps:
            await bot.edit_permissions(event.chat_id, user, view_messages=False, until_date=datetime.now() + timedelta(minutes=2))
            await bot.send_message(row[0], f"Kicked {user.first_name} {user.last_name} for not having a pfp")
    logging.info(f"[{chat.title}] Done")


@bot.on(events.NewMessage(pattern="/del"))
async def delete_message(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return await event.reply("Admins only, thanks")
    msg = await event.get_reply_message()
    await msg.delete()
    m = await event.respond("Message deleted")
    await asyncio.sleep(5)
    await m.delete()


@bot.on(events.NewMessage(pattern="/debug"))
async def debug_message(event):
    sender = await event.get_sender()
    if not await is_admin(event, sender):
        return await event.reply("Admins only, thanks")
    msg = await event.get_reply_message()
    file = BytesIO(str(msg).encode("utf-8"))
    file.name = "message.txt"
    file.seek(0)
    await bot.send_file(event.chat_id, reply_to=event.message.id, file=file, force_document=True, attributes=[DocumentAttributeFilename("message.txt")])


bot.start()
bot.run_until_disconnected()
